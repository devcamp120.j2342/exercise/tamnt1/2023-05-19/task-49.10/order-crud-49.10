/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
const gDRINK_BASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/drinks";
const gVOUCHER_URL = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/";
const gCOLUMN_ID = {
    orderCode: 0,
    kichCo: 1,
    loaiPiza: 2,
    nuocUong: 3,
    thanhTien: 4,
    hoTen: 5,
    sdt: 6,
    trangThai: 7,
    action: 8
}
const gCOL_NAME = [
    "orderCode",
    "kichCo",
    "loaiPizza",
    "idLoaiNuocUong",
    "thanhTien",
    "hoTen",
    "soDienThoai",
    "trangThai",
    "action"
]
const gPizzaSize = [
    {
        kichCo: "S",
        duongKinh: "20",
        suon: 2,
        salad: "200",
        soLuongNuoc: 2,
        thanhTien: 150000

    },
    {
        kichCo: "M",
        duongKinh: "25",
        suon: 4,
        salad: "300",
        soLuongNuoc: 3,
        thanhTien: 200000

    },
    {
        kichCo: "L",
        duongKinh: "30",
        suon: 8,
        salad: "500",
        soLuongNuoc: 4,
        thanhTien: 250000

    },

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()

        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()
    }
    //lắng nghe các sự kiện
    _onEventListener() {
        $("#btn-filter").on('click', () => {
            const vTrangThai = $("#select-order").val();
            const vLoaiPizza = $("#select-pizza").val();
            this._filterOrderTable(vTrangThai, vLoaiPizza)

        })
        $("#btn-order").on('click', () => {
            this.vModal.openModal("create")
        })
    }
    //Hàm để lọc danh sách bảng
    _filterOrderTable(paramtTrangThai, paramLoaiPizza) {
        $('#table-order').DataTable().search(paramtTrangThai + " " + paramLoaiPizza).draw();

    }
    //Hàm gọi api lấy danh sách đơn hàng
    _getOrderList() {
        this.vApi.onGetDataOrder((paramOrderList) => {
            this._createOrderTable(paramOrderList)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createOrderTable(paramOrderList) {
           //Xóa tất cả dự liệu cũ khi render dữ liệu mới
        if ($.fn.DataTable.isDataTable('#table-order')) {
            $('#table-order').DataTable().destroy();
        }
        const vOrderTable = $("#table-order").DataTable({
            // Khai báo các cột của datatable
            "columns": [
                { "data": gCOL_NAME[gCOLUMN_ID.orderCode] },
                { "data": gCOL_NAME[gCOLUMN_ID.kichCo] },
                { "data": gCOL_NAME[gCOLUMN_ID.loaiPiza] },
                { "data": gCOL_NAME[gCOLUMN_ID.nuocUong] },
                { "data": gCOL_NAME[gCOLUMN_ID.thanhTien] },
                { "data": gCOL_NAME[gCOLUMN_ID.hoTen] },
                { "data": gCOL_NAME[gCOLUMN_ID.sdt] },
                { "data": gCOL_NAME[gCOLUMN_ID.trangThai] },
                { "data": gCOL_NAME[gCOLUMN_ID.action] }
            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    "targets": gCOLUMN_ID.action,
                    "defaultContent": `
                    <img class="edit-order" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 25px;cursor:pointer; ">
                    <img class="delete-student" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 25px;cursor:pointer;">
                         `,

                    //Hàm click vào nút chi tiết hiển thị ra modal
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).on("click", ".edit-order", () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal("edit", vData)
                            })

                            $(cell).on("click", ".delete-student", () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal("delete", vData)
                            })
                        }

                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramOrderList) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
    }

    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._onEventListener()
        this._getOrderList()
    }
}
/*** REGION 3 - vùng để render ra các modal*/
class Modal {
    constructor() {
        this.vApi = new CallApi()
    }
    // hàm lắng nghe sự kiện tại modal
    _onModalEventListner(paramOrderDetail = null) {
        const vOrderId = paramOrderDetail?.id
        $("#order-status").on("change", (event) => {
            const vValue = event.target.value;
            $("#btn-update").on('click', () => {
                if (vValue === paramOrderDetail.trangThai) {
                    $("#btn-update").prop("disabled", true);
                }
                if (vValue === "confirmed") {
                    this.vApi.onBtnConfirmClick(vValue, vOrderId)
                } else {
                    this.vApi.onBtnCancelClick(vValue, vOrderId)
                }
            })
        })
        $("#select-create-size").on("change", (e) => {
            const vSize = e.target.value
            this._fillInputBaseOnSize(vSize)
        });
        $("#create-voucher").on("change", (event) => {
            const vPromCode = event.target.value
            if (!vPromCode) {
                this._clearInValid();
                return;
            }
            this._checkValidatePromCode(vPromCode)
        });
        $('#create-order-form').submit((event) => {
            event.preventDefault();
            this._createOrder();
        });
        $('#create-email').on("blur", (event) => {
            const vValue = event.target.value
            this._checkValidEmail(vValue)
        })

        $('#delete-order').on("click", () => {
            this.vApi.onBtnDeleteOrderClick()
        })

    }
    //Hàm kiểm tra mã code hợp lệ
    _checkValidatePromCode(paramPromoCode) {
        const vPromoCodeInput = $("#create-voucher");
        this._clearInValid(vPromoCodeInput)
        this.vApi.onCheckVoucherIdClick(paramPromoCode, (paramData) => {
            if (paramData === "invalid") {
                vPromoCodeInput.addClass('is-invalid');
                vPromoCodeInput.removeClass('border-success');
                vPromoCodeInput.after(`<div class="invalid-feedback error-message"> Mã không đúng, vui lòng nhâp mã khác hoặc để trống!</div>`)

            } else {
                vPromoCodeInput.removeClass('is-invalid');
                vPromoCodeInput.addClass('is-valid border-success')
                vPromoCodeInput.next('.invalid-feedback').html('');
                const bThanhTien = $("#create-total").val();
                const bphanTramGiaGiam = +paramData.phanTramGiamGia;
                // kiểm tra phần trăm mã giảm giá và tính tổng thành tiền
                if (bThanhTien) {
                    if (bphanTramGiaGiam >= 0 && bphanTramGiaGiam <= 100) {
                        const bDiscountPrice = (bphanTramGiaGiam / 100) * bThanhTien;
                        $('#create-discount').val(bDiscountPrice);
                    } else {
                        $('#create-discount').val(0);
                    }
                }
            }
        });
    }
    //Hàm kiểm tra email hợp lệ hay không
    _checkValidEmail(paramValue) {
        if (!paramValue) {
            this._clearInValid();
            return;
        }
        this._clearInValid()
        if (!paramValue.includes("@")) {
            $('#create-email').addClass("is-invalid");
            $('#create-email').after(`<div class="invalid-feedback error-message">Vui lòng nhập email hợp lệ.</div>`);
        }
    }
    //Hàm chọn size sẽ điền các phần còn lại
    _fillInputBaseOnSize(paramSize) {
        gPizzaSize.forEach((pizzaSize) => {
            if (pizzaSize.kichCo === paramSize) {
                $("#create-duong-kinh").val(pizzaSize.duongKinh);
                $("#create-suon").val(pizzaSize.suon);
                $("#create-salad").val(pizzaSize.salad);
                $("#create-pizza-type").val(pizzaSize.loaiPizza);
                $("#create-total").val(pizzaSize.thanhTien);
                $("#create-drink").val(pizzaSize.soLuongNuoc);
            }
        })
    }
    //Hàm tạo order
    _createOrder() {
        this._clearInValid()
        let isValid = true;
        const vKichCo = $("#select-create-size").val();
        const vDuongKinh = $("#create-duong-kinh").val();
        const vSuon = $("#create-suon").val();
        const vSalad = $("#create-salad").val();
        const vLoaiPizza = $("#select-create-type").val();
        const vIdVourcher = $("#create-voucher").val();
        const vSoLuongNuoc = $("#create-drink").val();
        const vThanhTien = $("#create-total").val();
        const vIdLoaiNuocUong = $("#select-create-drink").val();
        const vHoTen = $("#create-name").val();
        const vEmail = $("#create-email").val();
        const vSoDienThoai = $("#create-phone").val();
        const vDiaChi = $("#create-address").val();
        const vLoiNhan = $("#create-message").val();
        const vFields = ["select-create-drink","select-create-size", "select-create-type", "create-name", "create-phone", "create-address",]
        //Hàm kiểm tra trước lúc submit các input có hợp lệ không
        vFields.forEach((field) => {
            if (!$(`#${field}`).val().trim()) {
                isValid = false
                $(`#${field}`).addClass("is-invalid");
                $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
            }
        })
        if (isValid) {
            if (vThanhTien) {

            }
            const bCreateOrder = {
                kichCo: vKichCo,
                duongKinh: vDuongKinh,
                suon: +vSuon,
                salad: vSalad,
                loaiPizza: vLoaiPizza,
                idVourcher: vIdVourcher,
                idLoaiNuocUong: vIdLoaiNuocUong,
                soLuongNuoc: +vSoLuongNuoc,
                hoTen: vHoTen,
                thanhTien: vThanhTien,
                email: vEmail,
                soDienThoai: vSoDienThoai,
                diaChi: vDiaChi,
                loiNhan: vLoiNhan,
            };
            this.vApi.onCreateOrderClick(bCreateOrder)
        }
    }
    //Hàm gọi lấy các loại nước ra
    _getDrinkList() {
        this.vApi.onGetDrinkListClick((paramDrinkList) => {
            this._renderDrinks(paramDrinkList)
        })
    }

    //Hàm render các loại nước ra
    _renderDrinks(paramDrinkList) {
        const vSelect = $('#select-create-drink');
        vSelect.empty();
        vSelect.append($('<option>', {
            value: '',
            text: 'Chọn nước'
        }));

        paramDrinkList.forEach((paramDrink) => {
            vSelect.append($('<option>', {
                value: paramDrink.maNuocUong,
                text: paramDrink.tenNuocUong
            }));
        });
    }
    //Hàm xóa data ra khỏi bảng 
    _deleteOrder(paramOrderId) {
        $("#btn-delete-order").click(() => {
            this.vApi.onBtnDeleteOrderClick(paramOrderId)
        })
    }
    //Hàm hiển thị chi tiết thông tin của 1 order
    _renderOrderDetail(paramOrderId) {
        this.vApi.onGetOrderByOrderCode(paramOrderId, (paramOrderDetail) => {
            $("#order-code").val(paramOrderDetail.orderCode);
            $("#select-pizza-size").val(paramOrderDetail.kichCo);
            $("#order-duong-kinh").val(paramOrderDetail.duongKinh);
            $("#order-suon").val(paramOrderDetail.suon);
            $("#order-salad").val(paramOrderDetail.salad);
            $("#order-pizza-type").val(paramOrderDetail.loaiPizza);
            $("#order-voucher").val(paramOrderDetail.idVourcher);
            $("#order-total").val(paramOrderDetail.thanhTien);
            $("#order-discount").val(paramOrderDetail.giamGia);
            $("#select-drink").val(paramOrderDetail.idLoaiNuocUong);
            $("#order-drink").val(paramOrderDetail.soLuongNuoc);
            $("#order-name").val(paramOrderDetail.hoTen);
            $("#order-email").val(paramOrderDetail.email);
            $("#order-phone").val(paramOrderDetail.soDienThoai);
            $("#order-address").val(paramOrderDetail.diaChi);
            $("#order-message").val(paramOrderDetail.loiNhan);
            $("#order-status").val(paramOrderDetail.trangThai);
        });
    }
    //Hàm để xóa các trường tô đỏ không hợp lệ
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }
    //Hàm để xóa các trường input về giá trị rỗng
    _clearInput() {
        $("#create-order-form").find("input").val("");
        $("#create-order-form").find("select").val("")
    }
    //Hàm hiển thị ra các modal dựa vào điều kiện
    openModal(type, paramOrderDetail = null) {
        if (paramOrderDetail && type === "edit") {
            this._renderOrderDetail(paramOrderDetail.orderCode);
            this._onModalEventListner(paramOrderDetail);
            $("#modal-order-detail").modal('show');
        }
        if (paramOrderDetail && type === "delete") {
            $("#delete-order").modal('show');
            this._deleteOrder(paramOrderDetail.id)

        }
        if (type === "create") {
            this._clearInValid();
            this._clearInput();
            $("#modal-create-order").modal('show');
            this._onModalEventListner();
            this._getDrinkList()
        }

    }
}
/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    //Hàm show thông tin khi thực hiện CRUD
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }
    //Hàm confirm order
    onBtnConfirmClick(paramValue, paramOrderId) {
        const vStatus = {
            trangThai: paramValue,
        }
        $.ajax({
            url: `${gBASE_URL}/${paramOrderId}`,
            type: "PUT",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(vStatus),
            success: () => {
                const page = new RenderPage()
                page.renderPage()
                this.onShowToast("Confirm", "Bạn đã xác nhận đơn hàng!!")
                $("#modal-order-detail").modal('hide');
            },
            error: function (err) {
                console.log(err.status);
            }
        });
    }
    //Hàm hủy đơn hàng
    onBtnCancelClick(paramValue, paramOrderId) {
        const vStatus = {
            trangThai: paramValue,
        }
        $.ajax({
            url: `${gBASE_URL}/${paramOrderId}`,
            type: "PUT",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(vStatus),
            success: () => {
                const page = new RenderPage()
                page.renderPage()
                this.onShowToast("Cancel", "Bạn đã hủy nhận đơn hàng!!")
                $("#modal-order-detail").modal('hide');
            },
            error: function (err) {
                console.log(err.status);
            }
        });
    }
    //Hàm lấy đơn hàng từ mã order
    onGetOrderByOrderCode(paramOrderCode, paramCallbackFn) {
        $.ajax({
            url: `${gBASE_URL}/${paramOrderCode}`,
            type: "GET",
            success: function (response) {
                paramCallbackFn(response);

            },
            error: function (err) {
                console.log(err.status)
            }
        })
    }
    //Hàm lấy danh sách đơn hàng
    onGetDataOrder(paramCallbackFn) {
        $.ajax({
            url: gBASE_URL,
            type: "GET",
            success: function (response) {
                paramCallbackFn(response)
            },
            error: function (err) {
                console.log(err.status);
            }
        });
    }
      //Hàm lấy danh sách nước uống
    onGetDrinkListClick(paramCallbackFn) {
        $.ajax({
            url: gDRINK_BASE_URL,
            type: "GET",
            success: function (response) {
                paramCallbackFn(response)
            },
            error: function (xhr, status, error) {
                console.error(error);
            }
        });
    }
    //Hàm tạo đơn hàng
    onCreateOrderClick(paramOrder) {
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            data: JSON.stringify(paramOrder),
            contentType: "application/json;charset=UTF-8",
            success: () => {
                $("#modal-create-order").modal('hide');
                this.onShowToast("Đặt thành công", "Chúc mừng bạn đã đặt thành công")

            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
    //Hàm xóa đơn hàng
    onBtnDeleteOrderClick(paramOrderId) {
        $.ajax({
            url: gBASE_URL + "/" + paramOrderId,
            type: "DELETE",
            success: () => {
                const page = new RenderPage()
                page.renderPage()
                this.onShowToast("Xóa", "Bạn đã xóa order thành công")
                $("#delete-order").modal('hide');

            },
            error: () => {
                // Handle error if delete request fails
                console.log("Error deleting order " + vId);
            }
        });
    }
    //Hàm kiểm tra voucher
    onCheckVoucherIdClick(paramPromoCode, paramCallbackFn) {
        // một số mã đúng để test: 95531, 81432,...lưu ý test cả mã sai
        $.ajax({
            url: gVOUCHER_URL + paramPromoCode,
            method: "GET",
            success: function (data) {
                paramCallbackFn(data)
            },
            error: function (jqXHR, textStatus, errorThrown) {
                paramCallbackFn("invalid")
            }
        });
    }

}